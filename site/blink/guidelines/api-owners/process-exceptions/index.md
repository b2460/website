---
breadcrumbs:
- - /blink
  - Blink (Rendering Engine)
- - /blink/guidelines
  - Guidelines and Policies
page_name: process-exceptions
title: Exceptions to the feature-launching process
---

The [TAG review exceptions](/blink/launching-features/wide-review#exceptions)
have moved to the [guide on wide review for launching
features](/blink/launching-features/wide-review).
