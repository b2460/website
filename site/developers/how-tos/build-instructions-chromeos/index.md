---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/how-tos
  - How-Tos
page_name: build-instructions-chromeos
title: Build Instructions (Chromium OS on Linux)
---

This page has been replaced by
<https://chromium.googlesource.com/chromium/src/+/HEAD/docs/chromeos_build_instructions.md>

## Running Chromium on a Chromium OS device

See [Building Chromium for Chromium OS (simple
chrome)](/chromium-os/developer-library/guides/development/simple-chrome-workflow).
