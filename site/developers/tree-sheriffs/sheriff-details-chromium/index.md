---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/tree-sheriffs
  - Tree Sheriffs
page_name: sheriff-details-chromium
title: 'Sheriff Details: Chromium'
---

This page has been moved under [Chromium Trunk
Sheriffing](http://go/chrome-trunk-sheriffing) and [Chrome
Sheriffing How-To](http://go/chrome-sheriffing-how-to).

Contacting Sheriffs

The currently oncall sheriffs can be viewed in the top-left corner of the
[Chromium Main Console](https://ci.chromium.org/p/chromium/g/main/console). You
can also get in touch with sheriffs using the [#sheriffing Slack
channel](https://chromium.slack.com/messages/CGJ5WKRUH/).
