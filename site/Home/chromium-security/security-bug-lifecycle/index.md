---
breadcrumbs:
- - /Home
  - Chromium
- - /Home/chromium-security
  - Chromium Security
page_name: security-bug-lifecycle
title: Security Bug Lifecycle
---

The [canonical version of this document is now in the Chromium source
tree](https://chromium.googlesource.com/chromium/src/+/HEAD/docs/security/shepherd.md).
