---
breadcrumbs:
- - /chromium-os/developer-library/guides
  - ChromiumOS > Guides
page_name: git-helpers
title: Git helpers
---

## Tech Talks

*   Randall Schwartz's [Tech Talk on
            git](https://youtube.com/watch?v=8dhZ9BXQgc4)
*   [gitcasts](https://www.google.com/url?q=http%3A%2F%2Fgitcasts.com%2F&sa=D&sntz=1&usg=AFrqEzdXGanWebnB47ED_tYigjprxxyXlA)

## Online References

*   [Git
            Magic](https://www.google.com/url?q=http%3A%2F%2Fwww-cs-students.stanford.edu%2F%257Eblynn%2Fgitmagic%2F&sa=D&sntz=1&usg=AFrqEzd0LcYKlFSHeWlQzalvRlf4sTBLiA)
*   [Git for computer
            scientists](https://www.google.com/url?q=http%3A%2F%2Feagain.net%2Farticles%2Fgit-for-computer-scientists%2F&sa=D&sntz=1&usg=AFrqEzcOLmdu1_j4wvdnWaUkgTNwig-zxg)
*   [Git
            Manual](https://www.google.com/url?q=http%3A%2F%2Fwww.kernel.org%2Fpub%2Fsoftware%2Fscm%2Fgit%2Fdocs%2Fuser-manual.html&sa=D&sntz=1&usg=AFrqEzfv8SPESPQ4stacQ08cvUzlMXbbKA)
*   [git-scm.](https://www.google.com/url?q=http%3A%2F%2Fwww.git-scm.com%2F&sa=D&sntz=1&usg=AFrqEzfgZQlz4_hsTqn7WUSTXOLr3NjfHQ)[com](https://www.google.com/url?q=http%3A%2F%2Fwww.git-scm.com%2F&sa=D&sntz=1&usg=AFrqEzfgZQlz4_hsTqn7WUSTXOLr3NjfHQ)
*   [Git Visual Guide](http://marklodato.github.com/visual-git-guide/)

## Online Books

*   [Pro
            Git](https://www.google.com/url?q=http%3A%2F%2Fprogit.org%2Fbook%2F&sa=D&sntz=1&usg=AFrqEzd8OyxDVoHmTqpvytsEpbMxueIC2A)
*   [Git Community
            Book](https://www.google.com/url?q=http%3A%2F%2Fbook.git-scm.com%2F&sa=D&sntz=1&usg=AFrqEzeeHhoe_KJJGnPfXy5HvHYTkvCz0Q)
