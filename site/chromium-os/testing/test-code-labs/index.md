---
breadcrumbs:
- - /chromium-os
  - Chromium OS
- - /chromium-os/testing
  - Testing Home
page_name: test-code-labs
title: Test Code Labs
---

*   [Autotest Client
            Tests](/chromium-os/developer-library/training/codelabs/autotest-client-tests)
*   [Codelab for server side
            test](/chromium-os/developer-library/training/codelabs/server-side-test)
*   [Dynamic Suite
            Codelab](/chromium-os/developer-library/training/codelabs/dynamic-suite-codelab)
